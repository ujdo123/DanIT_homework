//Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// прототипное наследование дополняет созданній уже класс или обьект таким образом что не забивает память так, как если бі мі дублировали код
//Для чого потрібно викликати super() у конструкторі класу-нащадка?
// метод super() нужен для того что бі стянуть значения у наследованого класса

class Employee{
    constructor(name, age, salary){
        this.name = name,
        this.age = age,
        this.salary = salary
    }
    get Name(){
        return this.name;
    }
    set Name(name){
        this.name = name;
    }
    get Age(){
        return this.age;
    }
    set Age(age){
        this.age = age;
    }
    get Salary(){
        return this.salary;
    }
    set Salary(salary){
        this.salary = salary;
    }
}
class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }
    get Salary(){
        return this.salary * 3;
    }
}
const prog1 = new Programmer("Vlad", 18, 1000, "English");
const prog2 = new Programmer("Max", 24, 1500, "Ukrainian");
const prog3 = new Programmer("Pavel", 33, 3000, "Franche");
console.log(prog1);
console.log(prog1.Age);
console.log(prog1.Salary);
console.log(prog2);
console.log(prog2.Salary);
console.log(prog3);
console.log(prog3.Salary);