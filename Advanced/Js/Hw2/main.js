// try catch имеет смісл использовать при работе с запросами, обработки исключений
const parent = document.querySelector('#root')
const ul = document.createElement('ul')
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

parent.prepend(ul)
books.forEach(e => {
    try {
        if (!e.author) throw new Error('book dont have author')
        if (!e.name) throw new Error('book dont have name')
        if (!e.price) throw new Error('book dont have price')
        if ((e.author && e.name && e.price)) {
            let li = document.createElement('li')
            ul.append(li)
            li.innerText = `${e.author} ${e.name} ${e.price}`
        }
    } catch (error) {
        console.log(error.message);
    }
})
