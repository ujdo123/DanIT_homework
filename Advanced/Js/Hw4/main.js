//AJAX єто совокупность технологий вместе таких как html xml css js и тд, которіе помогает в динамике работать с сервером не перезагружая веб страницу
//как и в расшифровке Asynchronous JavaScript and XML говорит нам о том что js работает асинхоронно позволяя нам улучшить взаемодействие и скорость.
const container = document.querySelector('.container')
async function getInfo() {
    let response = await fetch('https://ajax.test-danit.com/api/swapi/films');
    let info = await response.json();
    return info;
}

function getEpisods(data) {
    return data.map((el) => {
        return {
            id: el.episodeId,
            name: el.name,
            description: el.openingCrawl
        };
    });
}

async function getCharacterNames(array, index) {
    const promises = array.map(async (el) => {
        const res = await fetch(el);
        const parsedRes = await res.json();
        return parsedRes.name;
    });

    const characterNames = await Promise.all(promises);
    const element = document.getElementById(index)
    element.innerHTML = characterNames;
    return characterNames;
}

function renderHTML(data) {
    if (!data.length) return;
    return data.forEach((el) => {
        container.insertAdjacentHTML('beforeend', `
    <ul>
      <li>${el.id} - ${el.name}</li>
      <li id="${el.id}"></li>
     <li>${el.description}</li>
    </ul>
    `);
    })
}

async function loadData() {
    const result = await getInfo();
    result.map((el) => getCharacterNames(el.characters, el.episodeId));
    renderHTML(getEpisods(result));
}

loadData();