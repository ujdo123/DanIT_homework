const container = document.querySelector('.container');
class Card {
    constructor(id, fullname, title, body, email) {
        this.id = id;
        this.fullname = fullname;
        this.title = title;
        this.body = body;
        this.email = email
    }
    render(container) {
        const item = document.createElement("div")
        item.innerHTML = `
                  <p>${this.fullname} - ${this.email}</p>
                  <p class="title">${this.title}</p>
                 <p>${this.body}</p>
                 <button class="item-delete">DELETE</button>
                `;
        item.querySelector('.item-delete').addEventListener('click',() => this.deletePost(item))
        container.append(item);
    }
    deletePost(item) {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
            method: "DELETE"
        })
            .then(response => {
                if (response.ok){
                    item.remove();
                    console.log("post was deleted");
                } 
            })
    }
}
async function getUsers() {
    const responseUsers = await fetch('https://ajax.test-danit.com/api/json/users');
    const parseResponseUsers = await responseUsers.json();
    return parseResponseUsers.map(e => {
        return {
            id: e.id,
            fullname: e.name,
            email: e.email
        }
    });
}
async function getPosts() {
    const responsePosts = await fetch('https://ajax.test-danit.com/api/json/posts')
    const parseResponsePosts = await responsePosts.json();
    return parseResponsePosts.map(e => {
        return {
            id: e.userId,
            title: e.title,
            body: e.body
        }
    });
}
async function renderCard() {
    result = await getUsers();
    resultPost = await getPosts();
    for (const post of resultPost) {
        let data = result.find(e => e.id === post.id);
        const card = getCard(post, data);
        card.render(container)
    }
}
function getCard(post, data) {
    ({ title, body, id } = post);
    ({ fullname, email } = data);
    return new Card(id, fullname, title, body, email)
}
renderCard();