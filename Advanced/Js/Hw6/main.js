// асинхронность позволяет делать віполнять код паролелльно віполниню остального кода, не запиная его, если надо использовать асинхронніе функции совместно нужно прокинуть их в промис что бі код отработал тогда когда функция біла віполнена.
const btnInfo = document.querySelector('#get-info')
const divInfo = document.querySelector('.info')
async function getData() {
    try{
    const response = await fetch('https://api.ipify.org/?format=json');
    const parseData = await response.json();
    return parseData.ip
    }catch(e){
        console.log("Error:" + e);
    }
}
async function getInfo(){
    try{
    const test = await getData();
    const ipApi = await fetch(`http://ip-api.com/json/${test}?fields=continent,country,region,city,currency,zip`);
    const parseIpApi = await ipApi.json()
    return parseIpApi
    }catch(e){
        console.log("Error:" + e);
    }
}
btnInfo.addEventListener('click', async () => {
    const info = await getInfo();
    divInfo.innerHTML= `
    <p>Continent: ${info.continent}</p>
    <p>Country: ${info.country}</p>
    <p>Region: ${info.region}</p>
    <p>City: ${info.city}</p>
    <p>Currency: ${info.currency}</p>
    <p>Zip: ${info.zip}</p> `
    btnInfo.style.display = 'none';
})
