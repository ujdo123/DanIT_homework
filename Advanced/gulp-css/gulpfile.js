const { src, dest, watch, parallel, series } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const webp = require('gulp-webp');
const newer = require('gulp-newer');
const htmlmin = require('gulp-htmlmin');
const html = () => {
    return src('./src/**/*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(dest('./dist'))
}
function styles() {
    return src('src/scss/**/*.scss')
        .pipe(autoprefixer({ overrideBrowserslist: ['last 10 version'] }))
        .pipe(concat('styles.min.css'))
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(dest('dist/css'))
        .pipe(browserSync.stream())
}
function scripts() {
    return src('src/js/**/*.js')
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(dest('dist/js'))
        .pipe(browserSync.stream())
}
function images(){
    return src (['src/img/**/*.*', '!src/img/**/*.svg'], {base: 'src/img'})
    .pipe(newer('dist/img/'))
    .pipe(webp())
    .pipe(dest('dist/img/'))
}
function watcher() {
    browserSync.init({
        server: {
            baseDir: 'dist/'
        }
    })
    watch(['src/scss/**/*.scss'], styles)
    watch(['src/js/**/*.js'], scripts)
    watch(['src/img/**/*.*'], images)
    watch(['src/**/*.html'], html).on('change', browserSync.reload)
}
function cleanDist() {
    return src('dist')
        .pipe(clean())
}
exports.html = html;
exports.styles = styles;
exports.scripts = scripts;
exports.images = images;
exports.watcher = watcher;
exports.cleanDist = cleanDist;
exports.build = series(cleanDist, parallel(images, html, styles, scripts, images));
exports.dev = parallel(styles, html, scripts, images, watcher)