const menuBurger = document.querySelector("#menuBurger");
const closeMenu = document.querySelector("#menuClose");
const menuList = document.querySelector("#menuList");
menuBurger.onclick = function () {
    menuBurger.classList.add("disable");
    closeMenu.classList.remove("disable");
    menuList.classList.remove("disable");
}
closeMenu.onclick = function () {
    closeMenu.classList.add("disable");
    menuList.classList.add("disable");
    menuBurger.classList.remove("disable");
}