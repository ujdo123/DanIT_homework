function tabs() {
    let contentWrapper = document.querySelectorAll('.content-text'),
        titleWrapper = document.querySelectorAll('.tabs-title'),
        dataTab;
    titleWrapper.forEach(e => e.addEventListener('click', clickTab));
    function clickTab() {
        titleWrapper.forEach(e => e.classList.remove("active"));
        this.classList.add("active");
        dataTab = this.getAttribute("data-tab-name");
        getContentText(dataTab);
    };
    function getContentText(dataTab) {
        contentWrapper.forEach(e => e.getAttribute("name") === dataTab ? e.classList.add('active') : e.classList.remove('active'))
    };
};
tabs();
