let password = document.getElementById('pass'),
passwordConfirm = document.getElementById('pass-confirm'),
formSubmit = document.querySelector('form'),
errorText = document.getElementById('error-text')
eye = document.getElementById('eye'),
eyeClose = document.getElementById('eyeclose');

function clickIcon(el){
    if(el.classList.contains('fa-eye')){
        el.classList.remove('fa-eye');
        el.classList.add('fa-eye-slash');
    }else{
        el.classList.remove('fa-eye-slash');
        el.classList.add('fa-eye');
    }
}
function checkInput(el){
    el.type === 'password' ? el.type = 'text' : el.type = 'password';
}
eye.addEventListener('click',(el) =>{
    clickIcon(el.target);
    checkInput(document.getElementById(el.target.getAttribute('data-id')))
})
eyeClose.addEventListener('click',(el) =>{
    clickIcon(el.target);
    checkInput(document.getElementById(el.target.getAttribute('data-id')))
})
formSubmit.addEventListener('submit', (event) => {
    if(password.value === passwordConfirm.value){
    errorText.innerText = '';
    alert('You are welcome');
    }else{
        errorText.innerText = 'Потрібно ввести однакові значення';
    }
    event.preventDefault()
})