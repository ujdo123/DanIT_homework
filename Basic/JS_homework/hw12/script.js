// Чому для роботи з input не рекомендується використовувати клавіатуру?
// потому что события клавиатуры нельзя считать например с голосового ввода или со вставки и тд.
const btn = document.querySelectorAll('.btn')
document.addEventListener('keypress', function (event) {
    if (event.key === "Enter") {
        colorChange(event.key)
    } else if (event.key === "s" || event.key === "e" || event.key === "o" || event.key === "n" || event.key == "l" || event.key === "z") {
        colorChange(event.key.toUpperCase())
    }
    function colorChange(el) {
        btn.forEach(e => {
            e.classList.remove('blue')
            e.innerHTML === el && e.classList.add('blue')
        })
    }
});
