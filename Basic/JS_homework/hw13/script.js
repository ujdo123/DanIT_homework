// 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval().
//  setTimeout() используется 1 раз через установленный промежуток времени, когда setInterval() используется n-раз с указанным интервалом

// 2.Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// setTimeout() будет выполнен после выполнения кода

// 3.Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// важно не забывать что в памяти хрянятся данные, и если не удалять таймеры, то могут быть баги или утечки памяти.

const btnStop = document.querySelector(".btn_stop"),
btnStart = document.querySelector(".btn_start");
let images = document.querySelectorAll(".image-to-show"),
slideIndex = 0,
maxIndex = images.length - 1,
timerId;
btnStart.disabled = true;
let showSlides = () => {
    timerId = setTimeout(() => {
        images[slideIndex].classList.toggle("hidden");
        images[slideIndex].classList.toggle("support-hidden");
        slideIndex = slideIndex === maxIndex ? 0 : ++slideIndex;
        images[slideIndex].classList.toggle("hidden");
        images[slideIndex].classList.toggle("support-hidden");
        showSlides();
    }, 3000);
};
showSlides();
btnStop.addEventListener("click", () => {
    clearTimeout(timerId);
    btnStart.removeAttribute("disabled");
    images[slideIndex].classList.remove("support-hidden");
    btnStop.setAttribute("disabled", true);
});
btnStart.addEventListener("click", () => {
    showSlides();
    btnStart.setAttribute("disabled", true);
    images[slideIndex].classList.add("support-hidden");
    btnStop.removeAttribute("disabled");
});

