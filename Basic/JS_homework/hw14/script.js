let icon = document.getElementById('moon')
if(localStorage.dark == "true"){
    document.body.classList.add("dark-theme")
    icon.src = "./img/sun.png"
} 
icon.onclick =function () {
    document.body.classList.toggle("dark-theme")
    if (document.body.classList.contains("dark-theme")) {
        localStorage.setItem("dark", "true")
        icon.src = "./img/sun.png"
    } else {
        localStorage.setItem("dark", "false")
        icon.src = "./img/moon-solid.svg"
    }
}