//Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?
//рекурсия выполняет возврат к функции, похоже на цикл, в моем случае рекурсия выполняется каждый раз пока выполняется условие true, и на условии false выход с рекурсии.

let number = +prompt ("Write your number")
while(isNaN(number) || !number || number % 1 !== 0 || number <= 0){
    number = +prompt ("Write your number")
}
let calcFactorial = function(number) {
    return (number != 1) ? number * calcFactorial(number - 1) : 1;
}
alert(calcFactorial(number));