let numberOne = +prompt("Write first number");
numberValidation(numberOne);
let numberTwo = +prompt("Write second number");
numberValidation(numberTwo);
let numberOrdinal = +prompt("Write ordinal number");
while (isNaN(numberOrdinal) || numberOrdinal % 1 !== 0 || numberOrdinal < 0) {
    numberOrdinal = +prompt("Write valid ordinal number");
}
function numberValidation(a) {
    while (isNaN(a) || a % 1 !== 0) {
        a = +prompt("Write valid number");
    }
}
function calcFibo(numberOne, numberTwo, numberOrdinal) {
    for (let i = 0; i < numberOrdinal; i++) {
        numberTwo = numberOne + numberTwo;
        numberOne = numberTwo - numberOne;
    }
    return numberOne;
}
alert(calcFibo(numberOne, numberTwo, numberOrdinal));
