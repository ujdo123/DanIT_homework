let user = {
    firstName: "Denis",
    lastName: "Korniienko",
    dob: "11.01.1999",
    "favorite hobbie": {
        ride: "street",
        walking: {
            street: true,
            forest: true,
        },
    },
    "favorite number": [11, 19, 27],
}
let cloneObject = function(object){
    return JSON.parse(JSON.stringify(object));
}
// ПРОВЕРКА 
cloneObjectOne = cloneObject(user);
cloneObjectOne.firstName = "test name";
console.log(user);
console.log(cloneObjectOne);