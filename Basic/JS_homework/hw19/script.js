let arrSpeedWork = [10, 8, 13, 14, 16];
let arrBackLog = [7, 12, 11, 15, 5];
let deadLine = new Date("2023, 06, 2, 19:00");
const calcWork = function (arrSpeedWork, arrBackLog, deadLine) {
    let date = new Date();
    let arrSumSpeedWork = arrSpeedWork.reduce((item, sum) => (item += sum)) / 8;
    let arrSumBackLog = arrBackLog.reduce((item, sum) => (item += sum));
    let workTime = 0;
    let workDay = 0;
    let chillTime = 1;
    const calcDiffDay = Math.floor((deadLine - date) * 2.77778e-7);
    while (arrSumBackLog >= arrSumSpeedWork) {
        workTime++ 
        arrSumSpeedWork += arrSpeedWork.reduce((item, sum) => (item += sum)) / 8;
    }
    let newWorkTime = workTime
    if (date.getHours() + workTime > 24 || (deadLine.getHours() - date.getHours()) < 8) {
        do{
            newWorkTime += 16;
            chillTime += 1;
        }
        while (workTime / 8 > chillTime)
    }
    if (date.getDay() === 6) {
        workDay = 2;
    } else if (date.getDay() === 0) workDay = 1;
    while ((deadLine.getDate() - date.getDate()) > workDay) {
        workDay++
    }
    if (deadLine.getDay() === 6) {
        workDay += 2;
    } else if (deadLine.getDay() === 0) workDay += 1;
    if (calcDiffDay > newWorkTime) {
        return `Усі завдання будуть успішно виконані за ${workDay} днів до настання дедлайну!`
    } else {
        newWorkTime -= calcDiffDay;
        return `Команді розробників доведеться витратити додатково ${newWorkTime} годин після дедлайну, щоб виконати всі завдання в беклозі`
    }
}
let functionComplite = calcWork(arrSpeedWork, arrBackLog, deadLine);
alert(functionComplite);