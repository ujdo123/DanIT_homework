//Опишіть своїми словами, що таке метод об'єкту
//методы обьекта это действия которые можно выполнить внутри обьекта, есть большое количество методов для обьектов, такие как defineProperties и тд.
// Який тип даних може мати значення властивості об'єкта?
// обьект может сохранять любой вид типов данных внутри себя.
// Об'єкт це посилальний тип даних. Що означає це поняття?
// это значит что все данные которые хранятся внутри обьекта считаються свойствами обьекта, и их индитификатором есть ключ, присвоев свойства одного обьекта к другому,
// если не поменять ключ свойсв, то они будут заменять друг друга.

let createNewUser = function () {
    let firstName = prompt("Your name");
    let lastName = prompt("Your last name");
    let newUser = {
        firstName,
        lastName,
        get getLogin() {
            return (this.firstName[0] + lastName).toLowerCase();
        }
    }
    Object.defineProperties(newUser, {
        firstName: {
            writable: false,
            get setFirstName() {
                return this.firstName;
            },
            set setFirstName(value) {
                this.firstName = value;
            },
        },
        lastName: {
            writable: false,
            get setLastName() {
                return this.lastName;
            },
            set setLastName(value) {
                this.lastName = value;
            },
        },
    })
    return newUser.getLogin;
}
console.log(createNewUser());