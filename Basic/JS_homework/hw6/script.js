//1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування

// экранирование нужно для того что бы использовать специальные символы как обычные, для того что бы использовать экранированые символы к ним нужно добавить \,
// так же благодоря экранированым символам можно делать валидацию.

//2. Які засоби оголошення функцій ви знаєте?
// Declaration и Expression, обьявить можно через вызов "function "variable"(){}", let "variable" = function(){}, "variable" = () =>, "variable" = () =>{}. последние 2 варианта являются Expression.

//3. Що таке hoisting, як він працює для змінних та функцій?
// возможность передать аргумент в параметр функции перед ее инициализацией, так же и для переменных если мы скажем что i = 1 а потом скажем var i и выведем в консоль то i будет равна 1, но так работает только с var
// let, const нужно обьявлять перед инициализацией.

let createNewUser = function () {
    let firstName = prompt("Your name");
    while (!firstName || !firstName.trim()) {
        firstName = prompt("Incorrect data");
    }
    let lastName = prompt("Your last name");
    while (!lastName || !lastName.trim()) {
        lastName = prompt("Incorrect data");
    }
    let birthday = prompt("When is your birthday?");
    while (!birthday || !birthday.match(/(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/)) {
        birthday = prompt("Incorrect data, try 'dd.mm.yyyy'");
    }
    let convertDate = birthday.substring(6,10) + birthday.substring(2,6) + birthday.substring(0,2);
    const actualDate = new Date() - new Date(convertDate);
    let newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge() {
            return Math.floor(actualDate / 3.15576e+10);
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
        },
    }
    Object.defineProperties(newUser, {
        firstName: {
            writable: false,
            get setFirstName() {
                return this.firstName;
            },
            set setFirstName(value) {
                this.firstName = value;
            },
        },
        lastName: {
            writable: false,
            get setLastName() {
                return this.lastName;
            },
            set setLastName(value) {
                this.lastName = value;
            },
        },
    })
    return newUser;
}
let newUser = createNewUser();
console.log(newUser.getAge());
console.log(newUser.getPassword());