//Опишіть, як можна створити новий HTML тег на сторінці.
//document.createElement\s, innerHTML

//Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
//первый параметр функции where говорит о том куда нужно произвести вставку например ("beforeBegin", "<p>hello</p>") вставит hello перед элементом к которому мы использовали данную функцию

//Як можна видалити елемент зі сторінки?
// remove(), removeChild(), ну или же передать пустую строку innerHTML = ''
let arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv", "1", "2", "3", "sea", [["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"]], "user", 23];
let parent = document.body
let createList = function (arr, parent) {
    let ul = document.createElement('ul')
    parent.prepend(ul)
    arr.forEach(element => {
        if(Array.isArray(element)){ 
            createList(element,ul)
        }else{
            let li = document.createElement('li')
            ul.append(li)
            li.innerText = element
        }
    })
}
createList(arr,parent)

let counter = 3;
let timer = document.createElement("p")
parent.append(timer)
const intervalId = setInterval(() => {
  timer.style.textAlign = 'center'
  timer.style.fontSize = '5em'
  timer.innerText = counter
  counter--;
if (counter < 0) {
    parent.remove()
    clearInterval(intervalId);
  }
}, 1000);