
// Our Services
let contentWrapper = document.querySelectorAll('.content-text'),
    titleWrapper = document.querySelectorAll('.tabs-title'),
    dataTab;
titleWrapper.forEach(e => e.addEventListener('click', clickTab));
function clickTab() {
    titleWrapper.forEach(e => e.classList.remove("active"));
    this.classList.add("active");
    dataTab = this.getAttribute("data-tab");
    contentWrapper.forEach(e => e.getAttribute("data-content") === dataTab ? e.classList.add('active') : e.classList.remove('active'))
};
// Our Amazing Work
const liItem = document.querySelectorAll('.work-tabs li'),
    imgItem = document.querySelectorAll('.work-list-content');
let loadMore = document.querySelector("#load-more"),
    currentItem = 12,
    dataName;
for (let i = 0; i < 12; i++) {
    if (imgItem[i]) imgItem[i].style.display = 'block';
}
let more = loadMore.onclick = (event) => {
    for (let i = currentItem; i < currentItem + 12; i++) {
        if (imgItem[i]) imgItem[i].style.display = 'block';
    }
    currentItem += 12;
    if (currentItem >= imgItem.length) {
        event.target.style.display = 'none'
    }
}
liItem.forEach(e => e.addEventListener('click', clickTabWork));
function clickTabWork() {
    liItem.forEach(e => e.classList.remove("active"));
    this.classList.add("active");
    dataName = this.getAttribute("data-filter");
    imgItem.forEach(e => e.style.display = 'none')
    for (let i = 0; i < imgItem.length; i++) {
        imgItem[i].style.display = 'none';
        if (imgItem[i].getAttribute('data-filter') === dataName) {
            loadMore.style.display = 'none'
            imgItem[i].style.display = 'block';
        } else if (dataName === "All") {
            imgItem[i].style.display = 'block';
            loadMore.style.display = 'block'
            if (i > 10) break;
        }
    }
    currentItem = 12;
};
//What People Say About theHam
let profileContent = document.querySelectorAll('.peopleSay-content'),
    profileImg = document.querySelectorAll(".peopleSay-profile"),
    currentPosition = 0;
profileList = document.querySelectorAll('.profile-list-content');
profileList.forEach(e => e.addEventListener('click', clickProfile));
function clickProfile() {
    profileList.forEach(e => e.classList.remove("active"));
    this.classList.add("active");
    profileContent.forEach(e => e.getAttribute("data-name") === this.getAttribute("data-name") ? e.classList.add("active") : e.classList.remove('active'))
    profileImg.forEach(e => e.getAttribute("data-name") === this.getAttribute("data-name") ? e.classList.add("active") : e.classList.remove('active'))
    position = Array.from(profileList).map(e => e.classList.contains("active"))
    currentPosition = +position.reduce(
        (out, bool, index) => bool ? out.concat(index) : out, "")
}
let arrProfile = document.querySelectorAll(".arr")
arrProfile.forEach(e => e.addEventListener("click", clickArr));
function clickArr(event) {
    if (event.currentTarget.classList.contains("left")) {
        profileList[currentPosition].classList.remove("active")
        profileContent[currentPosition].classList.remove("active")
        profileImg[currentPosition].classList.remove("active")
        if (currentPosition === 0) currentPosition = 4
        currentPosition -= 1;
        profileList[currentPosition].classList.add("active")
        profileContent[currentPosition].classList.add("active")
        profileImg[currentPosition].classList.add("active")
    } else {
        profileList[currentPosition].classList.remove("active")
        profileContent[currentPosition].classList.remove("active")
        profileImg[currentPosition].classList.remove("active")
        if (currentPosition === 3) currentPosition = -1
        currentPosition += 1;
        profileList[currentPosition].classList.add("active")
        profileContent[currentPosition].classList.add("active")
        profileImg[currentPosition].classList.add("active")
    }
}